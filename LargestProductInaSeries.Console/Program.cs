﻿using System;
using LargestProductInaSeries.Console.Exeptions;

namespace LargestProductInaSeries.Console
{
    class Program
    {
        private static void Main(string[] args)
        {

            try
            {
                var inputNumberOfTests = System.Console.ReadLine();
                var theNumberOfTest=ValidateNumberStr(inputNumberOfTests);
               

                while (theNumberOfTest > 0)
                {
                    var model = new LarguestNumbersViewModel
                    {
                        SequenceAndNumberOfDigits = System.Console.ReadLine(),
                        Number = System.Console.ReadLine()
                    };

                    model.Validate();

                    LargestProductInput.GetLarguestNumber(model);

                    theNumberOfTest--;
                }

            }
            catch (ModelIsNotValidException ex)
            {
                System.Console.WriteLine("The Model is not valid.");
            }
            System.Console.ReadLine();

        }

        private static int ValidateNumberStr(string input)
        {
            int theNumberOfTest;
            if (!int.TryParse(input, out theNumberOfTest))
                throw new ModelIsNotValidException();


            if (theNumberOfTest <= 0 || theNumberOfTest > 100)
                throw new ModelIsNotValidException();

            return theNumberOfTest;
        }

    }
}
