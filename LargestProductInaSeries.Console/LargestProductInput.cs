﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LargestProductInaSeries.Console
{
    public class LargestProductInput
    {
        public static void GetLarguestNumber(LarguestNumbersViewModel model)
        {

            var theConfiguration = model.SequenceAndNumberOfDigits;
            var values = theConfiguration?.Split(' ').Select(x => x.Trim()).ToArray();

            if (values?.Count() != 2) return;
            var thesequence = Convert.ToInt32(values[1]);
            var theNumberOfDigits = Convert.ToInt32(values[0]);

            var theNumberStr = model.Number;

            var listOfProductNumbers = new List<int>();

            for (var i = 0; i < thesequence; i++)
            {
                if (theNumberStr == null) continue;
                var theSubStringResult = theNumberStr.Substring(i);
                var currentSecuence = theSubStringResult.Select(x => x.ToString()).Take(thesequence).ToArray();

                listOfProductNumbers.Add(currentSecuence.Aggregate(1, (current, number) => current * Convert.ToInt32(number)));
            }

            System.Console.WriteLine(listOfProductNumbers.Max());
        }
    }
}