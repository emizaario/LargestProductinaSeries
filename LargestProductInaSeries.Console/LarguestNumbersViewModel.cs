﻿using System;
using System.Collections.Generic;
using System.Linq;
using LargestProductInaSeries.Console.Exeptions;

namespace LargestProductInaSeries.Console
{
    public class LarguestNumbersViewModel
    {

        public string NumberOfTest { get; set; }

        public string SequenceAndNumberOfDigits { get; set; }
        public string Number { get; set; }

        public void Validate()
        {
           

            var values = SequenceAndNumberOfDigits?.Split(' ').Select(x => x.Trim());

            if (values == null)
                throw new ModelIsNotValidException();


            var enumerable = values as string[] ?? values.ToArray();
            if (enumerable.ToArray().Count() != 2)
                throw new ModelIsNotValidException();

            var thesequence = Convert.ToInt32(enumerable.ToArray()[1]);
            var theNumberOfDigits = Convert.ToInt32(enumerable.ToArray()[0]);

            if (theNumberOfDigits < 1 || theNumberOfDigits > 1000)
                throw new ModelIsNotValidException();

            if (thesequence < 1 || thesequence > 7)
                throw new ModelIsNotValidException();


        }
    }


}