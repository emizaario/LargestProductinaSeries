﻿using System;
using LargestProductInaSeries.Console;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LargestProductInaSeries.UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void GetLarguestNumberIsWorking()
        {
            LargestProductInput.GetLarguestNumber(new LarguestNumbersViewModel
            {
                NumberOfTest = "1",
                SequenceAndNumberOfDigits = "10 5",
                Number = "3675356291"
            });
        }
    }
}
